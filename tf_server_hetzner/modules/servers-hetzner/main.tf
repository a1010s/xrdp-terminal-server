// ==== NETWORK ==== // 

# Create a Private Network
resource "hcloud_network" "private_net" {
  name     = "private_net"
  ip_range = var.ip_range
}

# Create a Subnet within the Private Network
resource "hcloud_network_subnet" "private_net_subnet" {
  network_id   = hcloud_network.private_net.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = var.ip_range
}

# Associate Servers with the Private Network's Subnet
resource "hcloud_server_network" "network_connection" {
  count     = var.instances
  server_id = hcloud_server.server[count.index].id
  subnet_id = hcloud_network_subnet.private_net_subnet.id
}

// ==== Send SSH-Keys to Servers ==== //

# Define an SSH Key to use with the servers
resource "hcloud_ssh_key" "default" {
  name       = "hetzner ssh pub-key"
  public_key = file("~/.ssh/hcloud-key.pub")
}

// ==== SERVERS ==== //

# Define Server resources
resource "hcloud_server" "server" {
  count       = var.instances
  name        = "terminal-server${count.index + 1}"
  server_type = var.type
  image       = var.image_name
  location    = var.location
  ssh_keys    = [
    hcloud_ssh_key.default.id,
  ]

  # Define the private IP of the Server's
  network {
    network_id = hcloud_network.private_net.id
    ip         = "10.0.0.${count.index + 5}"
  }
}
