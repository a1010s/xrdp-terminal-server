variable "instances" {
  default = "1"
}

variable "ip_range" {
  default = "10.0.0.0/24"
}

variable "location" {
  default = "nbg1"
}

variable "type" {
  description = "Hetzner Server Type"
  default = "cx11"
}

variable "image_name" {
  description = "Name of the server image"
  type        = string
  default     = "ubuntu:20.04"
}

variable "ssh_key_path" {
  description = "Path to the SSH public key"
  type        = list(string)
  default     = ["/.ssh/hcloud-key.pub"]
}

variable "server_count" {
  description = "Number of servers to be created"
  type        = number
}