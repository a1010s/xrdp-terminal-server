variable "hcloud_token" {
  description = "Hetzner Cloud API Key"
}

variable "type" {
  description = "The server type"
  default = "cx11"
}

variable "location" {
  description = "The server location"
  default = "nbg1"
}

variable "image_name" {
  description = "The name of the image to use"
  default = "ubuntu-20.04"
}

variable "ssh_key_path" {
  description = "Path to the SSH public key"
  type        = list(string)
  default     = ["/.ssh/hcloud-key.pub"]
}
