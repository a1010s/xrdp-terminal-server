terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.42.0"
    }
  }
}

# Input your API Token in variables.tf or when prompted
provider "hcloud" {
    token = var.hcloud_token
}


module "XRDP-Terminal-Server" {
  source       = "./modules/servers-hetzner"
  server_count = 1
  type  = var.type
  location = var.location
  image_name   = var.image_name
  ssh_key_path = var.ssh_key_path
}