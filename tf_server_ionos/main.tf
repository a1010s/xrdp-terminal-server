module "xrdp-server" {
  source       = "./modules/ionos-server"
  server_count = 1
  cores_count  = 1
  ram          = 1024
  cpu_family   = "AMD_OPTERON"
  image_name   = var.image_name
  ssh_key_path = var.ssh_key_path
}

output "server_primary_ip" {
  value = module.xrdp-server.server_primary_ip
}
