variable "cores" {
  description = "Number of CPU cores for the server"
  type        = number
  default     = 1
}

variable "ram" {
  description = "Amount of RAM in megabytes for the server"
  type        = number
  default     = 2048
}

variable "cpu_family" {
  description = "CPU family for the server"
  type        = string
  default     = "AMD_OPTERON"
}

variable "image_name" {
  description = "Name of the server image"
  type        = string
  default     = "ubuntu:latest"
}

variable "ssh_key_path" {
  description = "Path to the SSH public key"
  type        = list(string)
  default     = ["~/.ssh/id_rsa.pub"]
}

variable "cores_count" {
  description = "Number of CPU cores for the server"
  type        = number
  default     = 1
}

variable "server_count" {
  description = "Number of servers to be created"
  type        = number
}