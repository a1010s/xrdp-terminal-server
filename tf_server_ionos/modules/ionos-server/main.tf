output "server_primary_ip" {
  value = ionoscloud_server.server[*].primary_ip
}

resource "ionoscloud_datacenter" "datacenter" {
    name = "Test DC"
    location = "us/las"
}

resource "ionoscloud_lan" "pub-lan" {
  name          = "Public Lan Access"
  public        = true
  datacenter_id = ionoscloud_datacenter.datacenter.id
}



resource "ionoscloud_server" "server" {
  count         = var.server_count
  name          = "terminal-server${count.index + 1}"
  datacenter_id = ionoscloud_datacenter.datacenter.id
  cores         = var.cores_count
  ram           = var.ram
  cpu_family    = var.cpu_family
  image_name    = var.image_name
  ssh_key_path  = [pathexpand(var.ssh_key_path[0])]

  volume {
    name      = "vol${count.index + 1} OS"
    size      = 10
    disk_type = "SSD"
  }
  nic {
    lan  = ionoscloud_lan.pub-lan.id  # Use the lan_id variable here
    dhcp = true
  }
  lifecycle {
    ignore_changes = [nic, ssh_key_path]
  }
}