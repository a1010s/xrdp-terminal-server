variable "image_name" {
  default = "ubuntu:latest"
}

variable "ssh_key_path" {
  description = "Path to the SSH public key"
  type        = list(string)
  default     = ["~/.ssh/id_rsa.pub"]   # See pathexpander inside server
}

