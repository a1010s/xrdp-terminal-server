terraform {
  required_providers {
    ionoscloud = {
      source  = "ionos-cloud/ionoscloud"
      version = "= 6.2.5" #NOTE: v6.3.0. didn't intialize properly
    }
  }
}